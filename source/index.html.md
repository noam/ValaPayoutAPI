---
title: Vala Payout API Reference

language_tabs: # must be one of https://git.io/vQNgJ
  - json
  - shell

toc_footers:
  - <a href='https://www.valapay.com'>Valapay</a>

includes:
  - errors

search: true
---

# Introduction
Welcome to the Vala Payout API!

Partnering Financial institutions can integrate their system with the Vala Payout API to process
remittance payout transactions. Partners will be able to serve the designated beneficiaries of their
transactions via the Vala system, ensuring customers receive the proper payout.

## Overview

Vala is an operational system that enables management of all global money transfer activity for
remittance businesses. It is highly adaptive and can integrate with any financial partner. All transactions
occur online via the Vala web portal, which allows for cross platform integration across any operating
system. Customers can use a customized app while remittance businesses can manage all transactions
on the main dashboard.

This document is intended for programmers who work on behalf of partnering financial institutions
and are already familiar with REST API web services. 

### Request Response Structure

All methods are called via POST HTTP requests with custom HTTP headers and a JSON within the message
body. Parameters should be placed in the JSON body, with the signature included as an HTTP Header. 
When the Vala system is able to process an incoming request, a 200 (“Success”) HTTP code will be
returned and all response data will be provided within the response JSON. Other HTTP codes, detailed
below, will be used when the system is not able to process and respond to an incoming request.

### How It Works

When a sender decides to send money to a beneficiary, the first step is to go to the local sending Vala partner organization. 
A sender will deposit money at the organization, and is given a transaction ID number in return.
Then, a sender will give this number to a beneficiary. 
The beneficiary will take this transaction ID to the local receiving Vala partner organization and ask for the transaction to be processed.
The receiving organization will call Get Payment Info, sending the Vala system the transaction ID number, 
and Vala will return transaction details enabling the receiving organization to verify the identity 
of the beneficiary and make the proper transaction. Once this occurs, successfully or unsuccessfully, 
the receiving organization will call Set Payment Status to let the Vala system know the state of the transaction.
 


# Authentication

> To authorize a request, add this code to the header:

```json
[
  {   
   "X-Vala-ID" : "example",
   "X-Vala-Auth" : "example"
  }
]
```

> Make sure to replace `example` with your Vala ID and digital signature

The body of any request to the API contains a JSON, which is signed with the Partner’s 
private key. A request will include two custom HTTP headers, X-Vala-Auth and X-Vala-ID. X-Vala-Auth contains 
the digital signature of the body. X-Vala-ID contains the partner-specific ID number 
that tells the Vala system which public key to use. If authentication fails or errors occur 
while processing client request, the system will generate exceptions and error details will 
be available in the JSON body of the response. 

<aside class="notice">
You must replace <code>example</code> with your personal Vala ID and digital signature.
</aside>

## SSH and Key Generation

In order to use the API, a partner must generate a public and a private key. 
Partners must send their public keys to Vala and in return will receive an associated Vala ID. 
This allows for partners' HTTP requests to be securely verified by the system. 
The instructions below detail how to create public and private keys.



> Unix/Linux/Mac sample code for generating public and private key pair:

```shell
    ssh-keygen -t rsa
    Generating public/private rsa key pair.
    Enter file in which to save the key (/home/user/.ssh/id_rsa): 
    Enter passphrase (empty for no passphrase): 
    Enter same passphrase again: 
    Your identification has been saved in /home/user/.ssh/id_rsa.
    Your public key has been saved in /home/user/.ssh/id_rsa.pub.
    The key fingerprint is:
    7a:dj:0a:c6:36:4e:3g:et:27:38:8x:34:44:9d:01:87 user@a
    The key's randomart image is:
    +---[RSA 2048]----+
    |    .      ..oo..|
    |   . . .  . .o.X.|
    |    . . o.  ..+ B|
    |   .   o.o  .+ ..|
    |    ..o.S   o..  |
    |     o + = +     |
    |      . o + o .  |
    |           . o   |
    |                 |
    +-----------------+
```
> The public key is now located in /home/user/.ssh/id\_rsa.pub and the private key is now located in /home/user/.ssh/id_rsa.

###Unix/Linux/Mac

1) Generate the RSA key pair

    On the client machine, run the following command
    <code>ssh-keygen -t rsa</code>
   
2) Store and protect the key pair

    Choose a secure location on your system to store your keys. 
    You also have the option to protect these files with a password (recommended).
   
3) Share the public key with Vala

    Email your public key to apiintegrationemail@valapay.com
   
4) Keep your private key to yourself

    Make sure to hide the private key and keep it secure so that only you have access to it. 
    If there is a security breach, repeat the above steps to regenerate a key pair and share your new public key with the Vala development team.
 
###Windows

1) Download PuTTY

    Download links:
  
     <a href = 'https://www.ssh.com/a/putty-0.70-installer.msi'>Any-bit processor</a>
  
     <a href = 'https://www.ssh.com/a/putty-64bit-0.70-installer.msi'>64-bit processor</a>  
  
     Additional help:
  
     <a href = 'https://www.ssh.com/ssh/putty/windows/install'>Installation Instructions</a>
  
2) Running PuTTYgen

    Go to Windows Start menu → All Programs → PuTTY → PuTTYgen

3) Generate RSA key pair

    Using SSH-2 (RSA) and 2048 bits, generate the key pair with the Generate button.
    PuTTYgen will request that you move your mouse around in a random pattern to be utilized in generating the random keys.

4) Store and protect the key pair

    When this process is complete, you will be prompted to store your keys. 
    Choose a secure location on your system to store your keys. 
    You also have the option to protect these files with a password (recommended).

5) Keep your private key to yourself

    Make sure to hide the private key and keep it secure so that only you have access to it. 
    If there is a security breach, repeat the above steps to regenerate a key pair and share your new public key with the Vala development team.

    <a href = 'https://youtu.be/-92wEg68SKQ'>Video Example</a>

# API Endpoints

## Get Payment Info

This endpoint retrieves all payment info from the Vala system.

### HTTP Request

`POST {{host}}/api/transaction/query`

> Request Example:

```json
[
  {
    "queryCode" : "144MB45540151"
  }
]
```

### Request Parameters

Parameter       | Description
----------------|-----------
queryCode | Provided to the receiver (beneficiary) by the sender for this specific remittance transaction.

> Response Example:

```json
[
  {
    "statusCode" : "0",
    "message" : null,
    "status" : "SUCCESS",
    "organizationId" : "7058BBAD-2C1B-496F-A800-DD319B521656",
    "reservationCode" : "144MB45540151",
    "transactionDate" : "2018-07-28 15:14",
    "payoutMethod" : "CASH",
    "recipientAmount" : 544.6,
    "transactionStatus" : "FAIL",
        "sender" : {
                          "firstName" : "sandow",
                          "middleName" : "ron",
                          "lastName" : "doodley",
                          "dateOfBirth" : "1975-02-19",
                          "city" : "London",
                          "address" : "7 Bickleigh House",
                          "country" : "GB",
                          "phoneNumber" : "+447123123123",
                          "idNumber" : "123456",
                          "idType" : "PASSPORT",
                          "idExpiryDate" : "2020-09-19",
                          "email" : "doodley@gmail.com",
                          "zipCode" : "444555",
                          "gender" : "M",
                          "idIssueByCountry" : "LK"
        },
        "recipient" : {
                          "firstName": "Abdul",
                          "middleName": "Zain",
                          "lastName": "Alayah",
                          "dateOfBirth": "1982-05-14",
                          "city": "Accra",
                          "address": "880 Juaso-Nsawam Rd",
                          "country": "GH",
                          "phoneNumber": "+233372512422",
                          "idNumber": "789101",
                          "idType": "ID",
                          "idExpiryDate": "2022-08-14",
                          "email": "AZAlayah@gmail.com",
                          "zipCode": "444555",
                          "gender": "M",
                          "idIssueByCountry": "GH"
                          "bankName": "my bank",
                          "branchName": "my branch",
                          "accountNumber": "022990000999"
        }
  }
]
```

### Transaction Response json
Parameter       | Description
----------------|-----------
statusCode | HTTP status code: 0 if SUCCESS, error code if FAIL
message | Description of error: null if SUCCESS, message if FAIL
status | HTTP request status: SUCCESS or FAIL
organizationId | The ID of the organization processing the transaction
reservationCode | The ID of the transaction
transactionDate | The date and time a transaction is requested and put into the system
payoutMethod | The method in which the transaction is paid: CASH or BANK\_ACCOUNT
transactionStatus | The status of the transaction: PRE\_TXN, PROCESSING, COMPLETE_SUCCESS, FAIL


### Sender Response json
Parameter       | Description
----------------|-----------
firstName | The sender's first name
middleName | (optional) The sender's middle name
lastName | The sender's last name
dateOfBirth | The sender's date of birth
city | The sender's city
address | The sender's address
country | The sender's country
phoneNumber | The sender's phone number
idNumber | The sender's ID number
idType | The sender's ID type: ID, PASSPORT, or WORK\_PERMIT
idExpiryDate | The date that the sender's ID expires
email | The sender's email
zipCode | The sender's zipCode
gender | The sender's gender: M or F
idIssueByCountry | The country that issued the sender's ID

### Recipient Response json
Parameter       | Description
----------------|-----------
firstName | The recipient's first name
middleName | (optional) The recipient's middle name
lastName | The recipient's last name
dateOfBirth | The recipient's date of birth
city | The recipient's city
address | The recipient's address
country | The recipient's country
phoneNumber | The recipient's phone number
idNumber | The recipient's ID number
idType | The recipient's ID type: ID, PASSPORT, or WORK\_PERMIT
idExpiryDate | The date that the recipient's ID expires
email | The recipient's email
zipCode | The recipient's zipCode
gender | The recipient's gender: M or F
idIssueByCountry | The country that issued the recipient's ID
bankName | The recipient's bank
branchName | The branch of the recipient's bank to receive the transaction
accountNumber | The recipient's bank account number

## Set Payment Status

This endpoint sets information about a specific transaction in the Vala system and returns a message object.

### HTTP Request

`POST {{host}}/api/pull/transaction/setStatus`

> Request Example:

```json
[
  {
    "transactionId" : "d6371caa-57e1-4118-8068-55c5fb434c07",
    "statusToSet" : "INFO_MISSING",
    "comments" : "missing recipient id number"
  }
]
```

### Request Parameters

Parameter       | Description
----------------|-----------
transactionId | The ID of the transaction
statusToSet | The status of the transaction: <br>Transaction has been payed out: COMPLETE\_SUCCESS <br> Transaction cannot be payed out due to missing info: INFO_MISSING
comments | (Optional) Explanation of status, such as the missing info for a transaction. We recommend you provide pre-defined messages to use here.

> Response Example:

```json
[
    {
      "statusCode" : 0,
      "message" : null,
      "status" : "SUCCESS",
      "organizationId" : "7058BBAD-2C1B-496F-A800-DD319B521656"
    }
]
```

### Message Response json
Parameter       | Description
----------------|-----------
statusCode | HTTP status code: 0 if SUCCESS, error code if FAIL
message | Description of error: null if SUCCESS, message if FAIL
status | HTTP request status: SUCCESS or FAIL
