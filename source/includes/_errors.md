# Errors

<aside class="warning">
An improperly formatted request will return an http status not equal to <code>200</code>
</aside>

<aside class="warning">
A system error will return an http status of <code>200</code>, 
but the status parameter of the returned json will be <code>FAIL</code>.
The message parameter of the returned json will give more details about the error.
</aside>



##System Errors

> System error example:

```json
[
  {
    "statusCode" : 404,
    "message" : "Invalid transaction id",
    "status" : "FAIL",
    "organizationId" : "7058BBAD-2C1B-496F-A800-DD319B521656"
  }
]
```

Error Code | Meaning
---------- | -------
400 | Bad Request -- Your request is invalid.
404 | Not Found -- The requested entity could not be found.
470 | Unavailable Action -- This action is not available for the given entity.


